(* In this module we define the vector data type, i.e. lists that keep track of the length as well *)

Set Implicit Arguments.

(* The vector data type *)

Inductive Vec (A : Type) : nat -> Type :=
| nil              : Vec A 0
| cons  {n : nat}  : A -> Vec A n -> Vec A (S n)
.

Arguments nil [A].

(* A traditional definition of the length *)
Fixpoint length {A : Type}{n : nat}(v : Vec A n) : nat :=
  match v with
    | nil       => 0
    | cons _ vp => S (length vp)
  end.

(* Make use of the dependent type nature of Vec *)
Definition len {A : Type}{n : nat}(v : Vec A n) : nat := n.

(* Exercise: prove that length and len are the same using tactics.  I
am giving a proof that uses the declarative syntax. *)

Fixpoint concat {A : Type}{n m : nat}(u : Vec A n)(v : Vec A m) : Vec A (n + m) :=
  match u with
    | nil => v
    | cons a up => cons a (concat up v)
  end.

Notation "[]"          := nil.
Notation "[ x ]"       := (cons x nil).
Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).
Notation "hd :: tl" := (cons hd tl).

Fixpoint le (m n : nat) : bool :=
  match m , n with
    | 0      , _  => true
    | S _  , 0    => false
    | S mp , S np => le mp np
  end.

Notation "a <= b" := (le a b).


Fixpoint insert  {m : nat}(a : nat)(v : Vec nat m) : Vec nat (S m) :=
  match v with
    | []         => [a]
    | (hd :: tl) => (fun (u : nat)(x : nat)(xs : Vec nat u)   =>  
                      match a <= x with
                        | true  => a :: (x :: xs)
                        | false => x :: (insert a xs)
                      end) _ hd tl
  end.

Eval compute  in insert 4 [2 ; 5].